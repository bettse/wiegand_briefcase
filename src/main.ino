#include "Arduino.h"
#include "secret.h"
#include <Wiegand.h>

// Green
#define DATA0 13
// White
#define DATA1 12

#define HOLD 14
#define LED 27

#define WIEGAND_LENGTH 26

#define GREEN LOW
#define RED HIGH

#define LOCK_PIN 15
#define LOCK_DELAY_MS (3 * 1000)

#define LOCKED LOW
#define UNLOCKED HIGH

struct Blink {
  bool state;
  uint8_t count;
};

struct Card {
  uint8_t bitLength;
  uint8_t data[4];
};

long previousMillis = 0;
long interval = 500;
long relockTime = 0;

Wiegand reader;

struct Card accessCard;
volatile struct Card presented;
volatile bool connected = false;

struct Blink blink = {false, 0};

void IRAM_ATTR pinStateChanged() {
  reader.setPin0State(digitalRead(DATA0));
  reader.setPin1State(digitalRead(DATA1));
}

void stateChanged(bool plugged, const char *message) { connected = plugged; }

void receivedData(uint8_t *data, uint8_t bits, const char *message) {
  uint8_t count = (bits + 7) / 8;
  presented.bitLength = bits;
  memcpy((void *)presented.data, data, count);
}

// Notifies when an invalid transmission is detected
void receivedDataError(Wiegand::DataError error, uint8_t *rawData,
                       uint8_t rawBits, const char *message) {}

void error(const __FlashStringHelper *err) {
  // while (1);
}

void setup() {
  Serial.begin(115200);

  pinMode(DATA0, INPUT_PULLDOWN);
  pinMode(DATA1, INPUT_PULLDOWN);
  pinMode(HOLD, OUTPUT);
  pinMode(LED, OUTPUT);
  pinMode(LOCK_PIN, OUTPUT);

  digitalWrite(HOLD, HIGH);
  digitalWrite(LED, RED);
  digitalWrite(LOCK_PIN, LOCKED);

  reader.onReceive(receivedData, "Card readed: ");
  reader.onReceiveError(receivedDataError, "Card read error: ");
  reader.onStateChange(stateChanged, "State changed: ");
  reader.begin(WIEGAND_LENGTH, false);
  attachInterrupt(digitalPinToInterrupt(DATA0), pinStateChanged, CHANGE);
  attachInterrupt(digitalPinToInterrupt(DATA1), pinStateChanged, CHANGE);
  pinStateChanged;

  accessCard.bitLength = 26;
  memcpy((void *)accessCard.data, accessData, sizeof(accessCard.data));

  Serial.println();
}

bool accessGranted() {
  if (presented.bitLength != accessCard.bitLength) {
    return false;
  }
  for (int i = 0; i < (presented.bitLength + 7) / 8; i++) {
    if (presented.data[i] != accessCard.data[i]) {
      return false;
    }
  }
  return true;
}

void loop() {
  unsigned long currentMillis = millis();
  noInterrupts();
  reader.flush();
  interrupts();

  if (presented.bitLength > 0) {
    Serial.print("Card presented: ");
    Serial.print("(");
    Serial.print(presented.bitLength);
    Serial.print(") ");
    for (int i = 0; i < (presented.bitLength + 7) / 8; i++) {
      Serial.print(presented.data[i] >> 4, HEX);
      Serial.print(presented.data[i] & 0xF, HEX);
    }
    Serial.println();

    if (accessGranted()) {
      Serial.println("Access granted");

      digitalWrite(LOCK_PIN, UNLOCKED);
      digitalWrite(LED, GREEN);
      relockTime = currentMillis + LOCK_DELAY_MS;
    } else {
      blink.count = 5;
      Serial.println("Access denied");
    }
    presented.bitLength = 0;
  }

  if (relockTime > 0 && currentMillis > relockTime) {
    Serial.println("Relocking");
    digitalWrite(LED, RED);
    digitalWrite(LOCK_PIN, LOCKED);
    relockTime = 0;
  }

  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    if (blink.count > 0) {
        digitalWrite(LED, blink.state ? RED : GREEN);
        blink.state = !blink.state;
        blink.count--;
    }
  }
}
