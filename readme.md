# wiegand_briefcase

I want to put an insert wiegand reader into a briefcase with an electromagnetic lock to secure it.


## Parts
 - Reader that outputs weigand
 - Electromagnetic lock (https://a.co/d/06WFKvZo)
 - Relay (https://a.co/d/0he9pX16)
 - 12V to 5V buck converter (https://a.co/d/04HPfWz7)
 - ESP32
 - Briefcase
 - Power supply

## Hookup guide
 - Data0 (green) to ESP 13
 - Data1 (white) to ESP 12
 - Relay to ESP 15

## Software

Build using PlatformIO.  Put a `secrets.h` file in the `include` directory with the following contents:

```c
// Currently only supports 26bit credential
uint8_t accessData[4] = {0xFF, 0xFF, 0xFF, 0xFF};
```

## Operation

The briefcase will be locked when the ESP32 is powered on.  When a valid wiegand credential is read, the relay will be triggered to unlock the briefcase for 30s.
